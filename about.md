---
layout: page
title: About
permalink: /about/
---

I'm a 18-year-old guy which plays around with iOS, OSX and Linux.
I do bug hunting in my free time and occasionally play CTFs.

If you like what I do, follow me on Twitter: [@L00P3R](https://twitter.com/L00P3R)

Feel free to DM me with any suggestions/comments/questions on my posts or just to tell me that
I'm a noob.

<3